import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:form_get_users_bloc/home/bloc/home_bloc.dart';

// This is the type used by the popup menu below.
enum MenuItems { filterEven, filterUneven, reset }

class HomePage extends StatefulWidget {
  HomePage({Key key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  HomeBloc _homeBloc;
  @override
  void dispose() {
    _homeBloc.close();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) {
        _homeBloc = HomeBloc();
        _homeBloc..add(GetAllUsersEvent());
        return _homeBloc;
      },
      child: Scaffold(
        appBar: AppBar(
          actions: [
            PopupMenuButton(
              onSelected: (value) {
                if (value == MenuItems.filterEven) {
                  _homeBloc.add(FilterUsersEvent(filterEven: true));
                } else if (value == MenuItems.filterUneven) {
                  _homeBloc.add(FilterUsersEvent(filterEven: false));
                } else
                  _homeBloc.add(GetAllUsersEvent());
              },
              itemBuilder: (context) => <PopupMenuEntry<MenuItems>>[
                const PopupMenuItem<MenuItems>(
                  value: MenuItems.filterEven,
                  child: Text('Filter Even'),
                ),
                const PopupMenuItem<MenuItems>(
                  value: MenuItems.filterUneven,
                  child: Text('Filter Odd'),
                ),
                const PopupMenuItem<MenuItems>(
                  value: MenuItems.reset,
                  child: Text('Reset Filter'),
                ),
              ],
            )
          ],
          title: Text('Lista de Usuarios'),
        ),
        body: BlocConsumer<HomeBloc, HomeState>(
          listener: (context, state) {
            if (state is ErrorState) {
              // SnackBar de Error
              Scaffold.of(context)
                ..hideCurrentSnackBar()
                ..showSnackBar(
                  SnackBar(content: Text('Error: ${state.error}')),
                );
            }
          },
          builder: (context, state) {
            if (state is ShowUsersState) {
              return RefreshIndicator(
                onRefresh: () async {
                  _homeBloc..add(GetAllUsersEvent());
                },
                child: ListView.separated(
                  separatorBuilder: (context, index) => Divider(
                    color: Colors.black,
                  ),
                  itemCount: state.userLists.length,
                  itemBuilder: (BuildContext context, int index) {
                    return ListTile(
                      isThreeLine: true,
                      title: Text(state.userLists[index].name),
                      subtitle: Text(
                        '${state.userLists[index].id} Username: ${state.userLists[index].username} Company: ${state.userLists[index].company.name} Streeet: ${state.userLists[index].address.street} Phone: ${state.userLists[index].phone}',
                        maxLines: 2,
                      ),
                      onTap: () {},
                    );
                  },
                ),
              );
            } else if (state is ShowFilteredUsersState) {
              return RefreshIndicator(
                onRefresh: () async {
                  _homeBloc..add(GetAllUsersEvent());
                },
                child: ListView.separated(
                  separatorBuilder: (context, index) => Divider(
                    color: Colors.black,
                  ),
                  itemCount: state.userLists.length,
                  itemBuilder: (BuildContext context, int index) {
                    return ListTile(
                      isThreeLine: true,
                      title: Text(state.userLists[index].name),
                      subtitle: Text(
                        ' ${state.userLists[index].id} Username: ${state.userLists[index].username} Company: ${state.userLists[index].company.name} Streeet: ${state.userLists[index].address.street} Phone: ${state.userLists[index].phone}',
                        maxLines: 2,
                      ),
                      onTap: () {},
                    );
                  },
                ),
              );
            } else if (state is LoadingState) {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
            return Center(
              child: MaterialButton(
                onPressed: () {
                  BlocProvider.of<HomeBloc>(context).add(GetAllUsersEvent());
                },
                child: Text('Intentar de nuevo'),
              ),
            );
          },
        ),
      ),
    );
  }
}
