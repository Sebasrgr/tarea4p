import 'package:flutter/material.dart';
import 'package:form_get_users_bloc/home/home_page.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _formKey = GlobalKey<FormState>();
  bool _isTextHidden = true;
  TextEditingController _userTextController;
  TextEditingController _passTextController;

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(24.0),
          child: Form(
            key: _formKey,
            child: Column(
              children: [
                // username TextField
                TextFormField(
                  controller: _userTextController,
                  decoration: InputDecoration(
                      prefixIcon: Icon(Icons.person),
                      hintText: 'Ingresar usuario'),
                  validator: (text) {
                    if (text.isEmpty)
                      return "Ingrese usuario";
                    else
                      return null;
                  },
                ),
                // password TextField
                TextFormField(
                  controller: _passTextController,
                  obscureText: _isTextHidden,
                  decoration: InputDecoration(
                      prefixIcon: Icon(Icons.lock),
                      suffixIcon: IconButton(
                        icon: Icon(_isTextHidden
                            ? Icons.visibility_off
                            : Icons.visibility),
                        onPressed: () {
                          setState(() {
                            _isTextHidden = !_isTextHidden;
                          });
                        },
                      ),
                      hintText: 'Ingresar contraseña'),
                  validator: (text) {
                    if (text.isEmpty)
                      return "Ingrese password";
                    else
                      return null;
                  },
                ),
                SizedBox(height: 24),
                MaterialButton(
                  onPressed: _openHomePage,
                  child: Text('Ingresar'),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  // validación para HomePage
  _openHomePage() {
    if (_formKey.currentState.validate()) {
      Navigator.of(context).push(MaterialPageRoute(
        builder: (context) => HomePage(),
      ));
    } else
      return Scaffold.of(context)
        ..hideCurrentSnackBar()
        ..showSnackBar(
            SnackBar(content: Text('Favor de ingresar su información')));
  }
}
